package dev.vrba.discord.reset.service

import dev.kord.core.Kord
import dev.kord.gateway.Intent
import dev.kord.gateway.Intents
import dev.vrba.discord.reset.configuration.DiscordConfiguration
import dev.vrba.discord.reset.listener.EventListener
import kotlinx.coroutines.runBlocking
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Service

@Service
class DiscordBotService(private val configuration: DiscordConfiguration, private val listeners: List<EventListener>) : CommandLineRunner {

    override fun run(vararg args: String?) {
        runBlocking {
            val client = Kord(configuration.token)

            client.registerEventListeners(listeners)
            client.login {
                intents = Intents(Intent.Guilds, Intent.GuildMessages)
            }
        }
    }

    private fun Kord.registerEventListeners(listeners: List<EventListener>) {
        listeners.forEach {
            it.register(this)
        }
    }
}