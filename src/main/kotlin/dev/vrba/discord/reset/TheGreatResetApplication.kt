package dev.vrba.discord.reset

import dev.vrba.discord.reset.configuration.DiscordConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties(DiscordConfiguration::class)
class TheGreatResetApplication

fun main(args: Array<String>) {
	runApplication<TheGreatResetApplication>(*args)
}
