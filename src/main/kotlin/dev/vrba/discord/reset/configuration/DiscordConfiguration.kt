package dev.vrba.discord.reset.configuration

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "discord")
data class DiscordConfiguration(
    val token: String,
    val channel: Long,
    val maxMessages: Long,
    val maxMinutes: Long,
)