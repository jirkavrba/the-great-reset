package dev.vrba.discord.reset.listener

import dev.kord.core.Kord

interface EventListener {
    fun register(client: Kord)
}