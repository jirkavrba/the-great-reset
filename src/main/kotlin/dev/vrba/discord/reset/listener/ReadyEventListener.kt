package dev.vrba.discord.reset.listener

import dev.kord.common.entity.Snowflake
import dev.kord.core.Kord
import dev.kord.core.entity.Message
import dev.kord.core.entity.channel.TextChannel
import dev.kord.core.event.gateway.ReadyEvent
import dev.kord.core.on
import dev.vrba.discord.reset.configuration.DiscordConfiguration
import org.springframework.stereotype.Component

@Component
class ReadyEventListener(private val configuration: DiscordConfiguration) : EventListener {

    override fun register(client: Kord) {
        client.on<ReadyEvent> {
            val channel = client.getChannelOf<TextChannel>(Snowflake(configuration.channel)) ?: throw IllegalArgumentException("The configured channel was not found.")
        }
    }

    private suspend fun findPinnedMessage(channel: TextChannel): Message? {
        return null
    }

}